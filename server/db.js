'use strict'

const pg = require('pg');
const valueSequelize = require('sequelize');

const client = new pg.Client({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_POSTGRES_PORT
});

client.connect()
    .then(db => {
        console.log('DB pg is connect');
        console.log('************************************');
    }).catch(err => console.error(err));

const sequelize = new valueSequelize(process.env.POSTGRES_DB, process.env.POSTGRES_USER, process.env.password, {
    host: process.env.POSTGRES_HOST,
    dialect: 'postgres',
    port: process.env.POSTGRES_POSTGRES_PORT,
    dialectOptions: {
        connectTimeout: 60000
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

const db = {}

db.valueSequelize = valueSequelize;
db.sequelize = sequelize;

db.user = require('./src/models/user.model')(sequelize, valueSequelize);
db.rol = require('./src/models/rol.model')(sequelize, valueSequelize);
db.producto = require('./src/models/producto.model')(sequelize, valueSequelize);
db.categoria = require('./src/models/categoria.model')(sequelize, valueSequelize);
db.image = require('./src/models/image.model')(sequelize, valueSequelize);
db.auditoria = require('./src/models/auditoria.model')(sequelize, valueSequelize);

db.dosificacion = require('./src/models/dosificacion.model')(sequelize, valueSequelize);
db.facturacion = require('./src/models/facturacion.model')(sequelize, valueSequelize);

// middle table

db.producto_categoria = require('./src/models/producto_categoria.model')(sequelize, valueSequelize);
db.user_rol = require('./src/models/user_rol.model')(sequelize, valueSequelize);

// Many to Many

db.user.belongsToMany(db.rol, { through: db.user_rol, onDelete: 'CASCADE', foreignKey: 'user_id'})
db.rol.belongsToMany(db.user, { through: db.user_rol, onDelete: 'CASCADE', foreignKey: 'rol_id'})

db.producto.belongsToMany(db.categoria, { through: db.producto_categoria, onDelete: 'CASCADE', foreignKey: 'producto_id'})
db.categoria.belongsToMany(db.producto, { through: db.producto_categoria, onDelete: 'CASCADE', foreignKey: 'categoria_id'})

// One to One

// Many to One or One to Many


// db.project_user = require('../server/src/models/project_user.model')(sequelize, valueSequelize);
// db.project_category = require('../server/src/models/project_category.model')(sequelize, valueSequelize);

// db.user_skill = require('../server/src/models/user_skill.model')(sequelize, valueSequelize);

// db.project.belongsToMany(db.user, { through: db.project_user, onDelete: 'CASCADE', foreignKey: 'project_id'})
// db.user.belongsToMany(db.project, { through: db.project_user, onDelete: 'CASCADE', foreignKey: 'user_id'})

// db.user.belongsToMany(db.skill, { through: db.user_skill, onDelete: 'CASCADE', foreignKey: 'user_id'});
// db.skill.belongsToMany(db.user, { through: db.user_skill, onDelete: 'CASCADE', foreignKey: 'skill_id'});

// db.project.belongsToMany(db.category, { through: db.project_category, onDelete: 'CASCADE', foreignKey: 'project_id'});
// db.category.belongsToMany(db.project, { through: db.project_category, onDelete: 'CASCADE', foreignKey: 'category_id'});

// db.project.hasMany(db.image, { onDelete: 'CASCADE' })
// db.image.belongsTo(db.project, { onDelete: 'CASCADE' })

module.exports = db;