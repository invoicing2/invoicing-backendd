const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const envs = require('./env');
const db = require('./db');
const data = require('./src/_helpers/data');
const fs = require('fs');

console.log('************************************');
console.log(
    ' env: ', process.env.NODE_ENV, '\n',
    'port:', process.env.PORT, '\n',
    'postgres host: ', process.env.POSTGRES_HOST, '\n',
    'postgres user: ', process.env.POSTGRES_USER, '\n',
    'postgres password: ', process.env.POSTGRES_PASSWORD, '\n',
    'postgres db: ', process.env.POSTGRES_DB, '\n',
    'postgres port: ', process.env.POSTGRES_PORT, '\n',
    'port to web: ', process.env.PORT_FRONTEND
);
console.log('************************************');

app.set('port', process.env.PORT || 3000);

// midlewares

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(morgan('dev'));
app.use(express.json());
app.use(cors({ origin: process.env.PORT_FRONTEND })) // connect to port Vue

// routes
app.use(require('./src/routes/user.router'));
app.use(require('./src/routes/producto.router'));
app.use(require('./src/routes/categoria.router'));
app.use(require('./src/routes/dosificacion.router'));
app.use(require('./src/routes/facturacion.router'));
app.use(require('./src/routes/image.router'));

app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
    console.log('************************************');
    db.sequelize.sync({ force: true }).then(() => {
        data.initialDataUser();
        console.log('Drop and resync with { force: true }');
    });
});