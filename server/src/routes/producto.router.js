'use strict'

const express = require('express');
const router =  express.Router();

const productoCtrl = require('../controllers/producto.controller');

router.get('/api/productos', productoCtrl.findAll);
router.post('/api/productos', productoCtrl.create);
router.put('/api/productos', productoCtrl.update);
router.get('/api/productos/:id', productoCtrl.findById);
router.delete('/api/productos/:id', productoCtrl.delete);
// router.post('/api/add-project-user', productoCtrl.addProjectUser);
// router.delete('/api/remove-project-user/:id', productoCtrl.removeProjectUser);

module.exports = router;