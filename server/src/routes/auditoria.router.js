'use strict'

const express = require('express');
const router =  express.Router();

const auditoriaCtrl = require('../controllers/auditoria.controller');

router.get('/api/auditorias', auditoriaCtrl.findAll);
router.get('/api/auditorias/:id', auditoriaCtrl.findById);

module.exports = router;