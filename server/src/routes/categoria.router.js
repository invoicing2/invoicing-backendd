'use strict'

const express = require('express');
const router =  express.Router();

const categoriaCtrl = require('../controllers/categoria.controller');

router.get('/api/categorias', categoriaCtrl.findAll);
router.post('/api/categorias', categoriaCtrl.create);
router.put('/api/categorias', categoriaCtrl.update);
router.get('/api/categorias/:id', categoriaCtrl.findById);
router.delete('/api/categorias/:id', categoriaCtrl.delete);

module.exports = router;