'use strict'

const express = require('express');
const router =  express.Router();

const facturacionCtrl = require('../controllers/facturacion.controller');

router.get('/api/facturacion', facturacionCtrl.findAll);
router.post('/api/facturacion', facturacionCtrl.create);
router.put('/api/facturacion', facturacionCtrl.update);
router.get('/api/facturacion/:id', facturacionCtrl.findById);
router.delete('/api/facturacion/:id', facturacionCtrl.delete);

module.exports = router;