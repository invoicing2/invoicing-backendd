'use strict'

const express = require('express');
const router =  express.Router();

const dosificacionCtrl = require('../controllers/dosificacion.controller');

router.get('/api/dosificaciones', dosificacionCtrl.findAll);
router.post('/api/dosificaciones', dosificacionCtrl.create);
router.put('/api/dosificaciones', dosificacionCtrl.update);
router.get('/api/dosificaciones/:id', dosificacionCtrl.findById);
router.delete('/api/dosificaciones/:id', dosificacionCtrl.delete);

module.exports = router;