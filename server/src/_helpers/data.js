'user strict'

const bcrypt = require('bcrypt');
const db = require('../../db');
const User = db.user;
const Rol = db.rol;

const dataUser = [
    {
        firstName: 'Ayar Iru',
        lastName: 'Perez Vargas',
        userName: 'mos1234',
        email: 'xhosone@gmail.com',
        password: '12345',
        status: true,
        role: ['ROL_ADMIN'],
        gender: 'MALE',
        birthDate: new Date(1980, 6, 20),
        phone: '78855720'
    },
    {
        firstName: 'Nelson Iru',
        lastName: 'Martes Vargas',
        userName: 'melson1234',
        email: 'nelson@gmail.com',
        password: '123453',
        status: true,
        role: ['ROL_ADMIN'],
        gender: 'MALE',
        birthDate: new Date(1980, 6, 20),
        phone: '78855720'
    }
];
const dataRol = [
    {rol: 'ADMINISTRADOR'},
    {rol: 'USUARIO'},
    {rol: 'CONTADOR'},
    {rol: 'VENDEDOR'},
    {rol: 'CLIENTE'}
];
exports.initialDataUser = function () {
    dataUser.forEach(user => {
        user.password = bcrypt.hashSync(user.password, 10);
        User.create(user);
    });
    dataRol.forEach(rol => {
        Rol.create(rol);
    }) 
}
