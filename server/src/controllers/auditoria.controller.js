'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const Auditoria = db.auditoria;
const Sequelize = require('sequelize');

const auditoriaCtrl = {};

auditoriaCtrl.findAll = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    try {
        const auditoria = await Auditoria.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]] }); 
        const pages = Math.ceil(auditoria.count / limit);
            const elements = user.count;
            res.status(200).json(
                {
                    elements,
                    page,
                    pageSize,
                    pages,
                    auditoria,
                }
            );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

auditoriaCtrl.findById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await Auditoria.findOne({ where: { id: id }});
        res.status(200).json(response);
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

module.exports = auditoriaCtrl;