'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const User = db.user;
const Rol = db.rol;
const UserRol = db.user_rol;
const Sequelize = require('sequelize');

const userCtrl = {};

userCtrl.findAll = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    try {
        const user = await User.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]], include: [{model: Rol}] }); 
        const pages = Math.ceil(user.count / limit);
            const elements = user.count;
            res.status(200).json(
                {
                    elements,
                    page,
                    pageSize,
                    pages,
                    user,
                }
            );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

userCtrl.create = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    req.body.password = '12345';
    console.log('READ DATA', req.body);
    try {
        const response = await User.create(req.body);
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

userCtrl.update = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    try {
        const [numberOfAffectedRows, affectedRows] = await User.update(datas, { where: { id: datas.id }, returning: true, plain: true }); 
        res.status(200).json(affectedRows.dataValues);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

userCtrl.findById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await User.findOne({ where: { id: id }, include: [{model: Rol}]});
        res.status(200).json(response);
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

userCtrl.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await User.destroy({ where: { id: id } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

// Actions for authentication

userCtrl.resetPasswordInit = (req, res) => {
    const email = req.body.email;
    const refreshToken = cryptoRandomString({ length: 50, type: 'base64' });
    Ususario.findOne({ where: { email: email } }).then((usuario) => {
        const url = 'http://localhost:8080/reset-password-finish/';
        mail.sendMail('/html/resetPassword.html', 'Recuperar Contraseña', url + refreshToken, usuario);
        res.status(200).json({ key: "Esta es la llave que se mandara" });
        const data = usuario.dataValues;
        data.reset_key = refreshToken;
        Usuario.update(data, { where: { id: usuario.id } }).then(() => {
            console.log('Se le envio una URL');
            res.status(200).json('Your account was updated successfully');
        });
    }).catch((err) => {
        res.status(500).json({ msg: 'error', details: err });
    });
}

userCtrl.resetPasswordFinish = (req, res) => {
    const key = req.body.key;
    const newPassword = req.body.newPassword;
    Usuario.findOne({ where: { reset_key: key } }).
        then((usuario) => {
            bcrypt.hash(newPassword, 10, function (err, hash) {
                const data = usuario.dataValues;
                data.reset_key = null;
                data.password = hash;
                Usuario.update(data, { where: { id: data.id } }).then(() => {
                    console.log('La contrasenia fue actualizada exitosamente');
                    res.status(200).json('Your account was updated successfully');
                });
            });
        }).catch((err) => {
            res.status(500).json({ msg: 'error', details: err });
        });
}

userCtrl.activateAccount = async (req, res, next) => {
    const key = req.body.key;
    const findByKey = await Usuario.findOne({ where: { activate_key: key } });
    if (findByKey !== null) {
        const data = await findByKey.dataValues;
        data.estado = true;
        data.activate_key = null;
        await Usuario.update(data, { where: { id: findByKey.id } })
        res.status(200).json(findByKey);
    } else {
        res.status(500).json({ msg: 'error', details: 'La llave no es valida' });
    }
}

userCtrl.addUserRol = async (req, res) => {
    try {
        console.log('---> ', req.body);
        const response = await UserRol.create({ user_id: req.body.userId, rol_id: req.body.rolId });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

userCtrl.removeUserRol = async (req, res) => {
    try {
        await UserRol.destroy({ where: { user_id: req.body.userId, rol_id: req.body.rolId } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}


module.exports = userCtrl;