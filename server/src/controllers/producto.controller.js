'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const Producto = db.producto;
const User = db.user;
const ProjectUser = db.project_user;
const Sequelize = require('sequelize');

const productoCtrl = {};

productoCtrl.findAll = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    console.log('=-------------->>>>>>> ', page)
    try {
        const producto = await Producto.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]]});
        const convert = producto.count / 10;
        const pages = convert > Math.round(convert) ? Math.round(convert) + 1 : Math.round(convert);
        console.log('Hello', producto);
        // const pages = Math.ceil(producto.count / limit);
        const elements = producto.count;
        const rows = producto.rows;
        // res.setHeader('x-total-count', elements);
        res.status(200).json(
            {
                elements,
                page,
                pageSize,
                pages,
                rows
            }
        );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

productoCtrl.create = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    req.body.password = '12345';
    console.log('READ DATA', req.body);
    try {
        const response = await Producto.create(req.body);
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

productoCtrl.update = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    try {
        const [numberOfAffectedRows, affectedRows] = await Producto.update(datas, { where: { id: datas.id }, returning: true, plain: true }); 
        res.status(200).json(affectedRows.dataValues);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

productoCtrl.findById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await Producto.findOne({ where: { id: id }});
        res.status(200).json(response);
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

productoCtrl.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Producto.destroy({ where: { id: id } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

// Added project user relationship

// productoCtrl.addProjectUser = async (req, res) => {
//     try {
//         console.log('---> ', req.body);
//         const response = await ProjectUser.create({ project_id: req.body.projectId, user_id: req.body.userId });
//         res.status(200).json(response);
//     } catch (error) {
//         res.status(500).json({ msg: 'error', details: error });
//     }
// }

// productoCtrl.removeProjectUser = async (req, res) => {
//     try {
//         await ProjectUser.destroy({ where: { userId: id, projectId: id } })
//         res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
//     } catch (error) {
//         res.status(500).json({ msg: 'error', details: error });
//     }
// }

module.exports = productoCtrl;