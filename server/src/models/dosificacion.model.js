'use strict'

module.exports = (sequelize, Sequelize) => {
    const dosificacion = sequelize.define('dosificacion', {
        nombre: {
            type: Sequelize.STRING
        },
        valorInicio: {
            type: Sequelize.INTEGER
        },
        valorFin: {
            type: Sequelize.INTEGER
        },
        numeroDosificacion: {
            type: Sequelize.INTEGER,
        },
        llaveDosificacion: {
            type: Sequelize.STRING
        },
        fechaLimiteEmission: {
            type: Sequelize.DATE
        },
        leyenda: {
            type: Sequelize.STRING
        },
        estado: {
            type: Sequelize.BOOLEAN
        }
    });
    return dosificacion
}