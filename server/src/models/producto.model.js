'use strict'

module.exports = (sequelize, Sequelize) => {
    const producto = sequelize.define('producto', {
        name: {
            type: Sequelize.STRING
        },
        descripcion: {
            type: Sequelize.STRING
        },
        unidades: {
            type: Sequelize.STRING
        },
        precioUnidad: {
            type: Sequelize.STRING
        }
    });
    return producto;
}