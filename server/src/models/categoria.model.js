'use strict'

module.exports = (sequelize, Sequelize) => {
    const categoria = sequelize.define('categoria', {
        titulo: {
            type: Sequelize.STRING
        },
        descripcion: {
            type: Sequelize.STRING
        }
    });
    return categoria
}