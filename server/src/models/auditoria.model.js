'use strict'

module.exports = (sequelize, Sequelize) => {
    const auditoria = sequelize.define('auditoria', {
        ip: {
           type: Sequelize.STRING
        }
    });
    return auditoria;
}