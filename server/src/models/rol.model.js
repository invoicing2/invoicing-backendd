'use strict'

module.exports = (sequelize, Sequelize) => {
    const rol = sequelize.define('rol', {
        rol: {
            type: Sequelize.STRING
        }
    });
    return rol;
}