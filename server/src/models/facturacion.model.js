'use strict'

module.exports = (sequelize, Sequelize) => {
    const facturacion = sequelize.define('facturacion', {
        nit: {
            type: Sequelize.INTEGER
        },
        razonSocial: {
            type: Sequelize.STRING
        },
        codigoControl: {
            type: Sequelize.INTEGER
        },
        fechaEmision: {
            type: Sequelize.DATE
        },
        numeroFactura: {
            type: Sequelize.INTEGER
        },
        montoTotal: {
            type: Sequelize.DOUBLE
        }
    })
    return facturacion;
}